<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A55596">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The speech of the recorder of Bristol to His Highness the Prince of Orange. Monday, January the 7th, 1688. The mayor, recorder, aldermen, and commons of the principal citizens, of the city of Bristol, waited upon the Prince of Orange, being introduc'd by His Grace the Duke of Ormond, their high-steward, and the Earl of Shrewsbury: where the recorder spake to this effect</title>
    <author>Bristol (England). Recorder.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A55596 of text R217038 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing P3115A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A55596</idno>
    <idno type="STC">Wing P3115A</idno>
    <idno type="STC">ESTC R217038</idno>
    <idno type="EEBO-CITATION">99828739</idno>
    <idno type="PROQUEST">99828739</idno>
    <idno type="VID">33170</idno>
    <idno type="PROQUESTGOID">2240869867</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A55596)</note>
    <note>Transcribed from: (Early English Books Online ; image set 33170)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1957:6)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The speech of the recorder of Bristol to His Highness the Prince of Orange. Monday, January the 7th, 1688. The mayor, recorder, aldermen, and commons of the principal citizens, of the city of Bristol, waited upon the Prince of Orange, being introduc'd by His Grace the Duke of Ormond, their high-steward, and the Earl of Shrewsbury: where the recorder spake to this effect</title>
      <author>Bristol (England). Recorder.</author>
      <author>Powlett, William. aut</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>[s.n.],</publisher>
      <pubPlace>London :</pubPlace>
      <date>printed in the year 1689.</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of the original in the Henry E. Huntington Library and Art Gallery.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>William, -- King of England, -- 1650-1702 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>The speech of the recorder of Bristol to His Highness the Prince of Orange. Monday, January the 7th, 1688. The mayor, recorder, aldermen, and commons</ep:title>
    <ep:author>Bristol . Recorder</ep:author>
    <ep:publicationYear>1689</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>414</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-04</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-04</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-05</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-05</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A55596-t">
  <body xml:id="A55596-e0">
   <div type="speech" xml:id="A55596-e10">
    <pb facs="tcp:33170:1" xml:id="A55596-001-a"/>
    <head xml:id="A55596-e20">
     <w lemma="the" pos="d" xml:id="A55596-001-a-0010">THE</w>
     <w lemma="speech" pos="n1" xml:id="A55596-001-a-0020">SPEECH</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-0030">OF</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-0040">THE</w>
     <hi xml:id="A55596-e30">
      <w lemma="recorder" pos="n1" xml:id="A55596-001-a-0050">Recorder</w>
      <w lemma="of" pos="acp" xml:id="A55596-001-a-0060">of</w>
      <w lemma="Bristol" pos="nn1" xml:id="A55596-001-a-0070">Bristol</w>
     </hi>
     <w lemma="to" pos="acp" xml:id="A55596-001-a-0080">To</w>
     <w lemma="his" pos="po" xml:id="A55596-001-a-0090">His</w>
     <w lemma="highness" pos="n1" xml:id="A55596-001-a-0100">HIGHNESS</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-0110">The</w>
     <w lemma="prince" pos="n1" xml:id="A55596-001-a-0120">Prince</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-0130">of</w>
     <w lemma="Orange" pos="nn1" xml:id="A55596-001-a-0140">Orange</w>
     <pc unit="sentence" xml:id="A55596-001-a-0150">.</pc>
    </head>
    <head type="sub" xml:id="A55596-e40">
     <date xml:id="A55596-e50">
      <w lemma="Monday" pos="nn1" xml:id="A55596-001-a-0160">Monday</w>
      <pc xml:id="A55596-001-a-0170">,</pc>
      <w lemma="January" pos="nn1" xml:id="A55596-001-a-0180">January</w>
      <hi xml:id="A55596-e60">
       <w lemma="the" pos="d" xml:id="A55596-001-a-0190">the</w>
       <w lemma="seven" pos="ord" xml:id="A55596-001-a-0200">7th</w>
       <pc xml:id="A55596-001-a-0210">,</pc>
      </hi>
      <w lemma="1688." pos="crd" xml:id="A55596-001-a-0220">1688.</w>
      <pc unit="sentence" xml:id="A55596-001-a-0230"/>
     </date>
    </head>
    <argument xml:id="A55596-e70">
     <p xml:id="A55596-e80">
      <hi xml:id="A55596-e90">
       <w lemma="the" pos="d" xml:id="A55596-001-a-0240">The</w>
       <w lemma="mayor" pos="n1" xml:id="A55596-001-a-0250">Mayor</w>
       <pc xml:id="A55596-001-a-0260">,</pc>
       <w lemma="Recorder" pos="nn1" xml:id="A55596-001-a-0270">Recorder</w>
       <pc xml:id="A55596-001-a-0280">,</pc>
       <w lemma="alderman" pos="n2" xml:id="A55596-001-a-0290">Aldermen</w>
       <pc xml:id="A55596-001-a-0300">,</pc>
       <w lemma="and" pos="cc" xml:id="A55596-001-a-0310">and</w>
       <w lemma="commons" pos="n2" xml:id="A55596-001-a-0320">Commons</w>
       <w lemma="of" pos="acp" xml:id="A55596-001-a-0330">of</w>
       <w lemma="the" pos="d" xml:id="A55596-001-a-0340">the</w>
       <w lemma="principal" pos="j" xml:id="A55596-001-a-0350">Principal</w>
       <w lemma="citizen" pos="n2" xml:id="A55596-001-a-0360">Citizens</w>
       <pc xml:id="A55596-001-a-0370">,</pc>
       <w lemma="of" pos="acp" xml:id="A55596-001-a-0380">of</w>
       <w lemma="the" pos="d" xml:id="A55596-001-a-0390">the</w>
       <w lemma="city" pos="n1" xml:id="A55596-001-a-0400">City</w>
       <w lemma="of" pos="acp" xml:id="A55596-001-a-0410">of</w>
      </hi>
      <w lemma="Bristol" pos="nn1" xml:id="A55596-001-a-0420">BRISTOL</w>
      <pc xml:id="A55596-001-a-0430">,</pc>
      <hi xml:id="A55596-e100">
       <w lemma="wait" pos="vvn" xml:id="A55596-001-a-0440">Waited</w>
       <w lemma="upon" pos="acp" xml:id="A55596-001-a-0450">upon</w>
       <w lemma="the" pos="d" xml:id="A55596-001-a-0460">the</w>
       <w lemma="prince" pos="n1" xml:id="A55596-001-a-0470">Prince</w>
       <w lemma="of" pos="acp" xml:id="A55596-001-a-0480">of</w>
      </hi>
      <w lemma="Orange" pos="nn1" xml:id="A55596-001-a-0490">ORANGE</w>
      <pc xml:id="A55596-001-a-0500">,</pc>
      <hi xml:id="A55596-e110">
       <w lemma="be" pos="vvg" xml:id="A55596-001-a-0510">being</w>
       <w lemma="introduce" pos="vvd" reg="introduced" xml:id="A55596-001-a-0520">introduc'd</w>
       <w lemma="by" pos="acp" xml:id="A55596-001-a-0530">by</w>
       <w lemma="his" pos="po" xml:id="A55596-001-a-0540">His</w>
       <w lemma="grace" pos="n1" xml:id="A55596-001-a-0550">Grace</w>
       <w lemma="the" pos="d" xml:id="A55596-001-a-0560">the</w>
       <w lemma="duke" pos="n1" xml:id="A55596-001-a-0570">Duke</w>
       <w lemma="of" pos="acp" xml:id="A55596-001-a-0580">of</w>
      </hi>
      <w lemma="ormond" pos="nn1" xml:id="A55596-001-a-0590">Ormond</w>
      <pc xml:id="A55596-001-a-0600">,</pc>
      <hi xml:id="A55596-e120">
       <w lemma="their" pos="po" xml:id="A55596-001-a-0610">their</w>
       <w lemma="high-steward" pos="n1" xml:id="A55596-001-a-0620">High-Steward</w>
       <pc xml:id="A55596-001-a-0630">,</pc>
       <w lemma="and" pos="cc" xml:id="A55596-001-a-0640">and</w>
       <w lemma="the" pos="d" xml:id="A55596-001-a-0650">the</w>
       <w lemma="earl" pos="n1" xml:id="A55596-001-a-0660">Earl</w>
       <w lemma="of" pos="acp" xml:id="A55596-001-a-0670">of</w>
      </hi>
      <w lemma="Shrewsbury" pos="nn1" xml:id="A55596-001-a-0680">Shrewsbury</w>
      <pc xml:id="A55596-001-a-0690">:</pc>
      <hi xml:id="A55596-e130">
       <w lemma="where" pos="crq" xml:id="A55596-001-a-0700">Where</w>
       <w lemma="the" pos="d" xml:id="A55596-001-a-0710">the</w>
       <w lemma="recorder" pos="n1" xml:id="A55596-001-a-0720">Recorder</w>
       <w lemma="speak" pos="vvd" reg="spoke" xml:id="A55596-001-a-0730">spake</w>
       <w lemma="to" pos="acp" xml:id="A55596-001-a-0740">to</w>
       <w lemma="this" pos="d" xml:id="A55596-001-a-0750">this</w>
       <w lemma="effect" pos="n1" xml:id="A55596-001-a-0760">effect</w>
       <pc xml:id="A55596-001-a-0770">:</pc>
      </hi>
     </p>
    </argument>
    <opener xml:id="A55596-e140">
     <salute xml:id="A55596-e150">
      <w lemma="may" pos="vmb" xml:id="A55596-001-a-0780">May</w>
      <w lemma="it" pos="pn" xml:id="A55596-001-a-0790">it</w>
      <w lemma="please" pos="vvi" xml:id="A55596-001-a-0800">please</w>
      <w lemma="your" pos="po" xml:id="A55596-001-a-0810">Your</w>
      <w lemma="highness" pos="n1" xml:id="A55596-001-a-0820">Highness</w>
      <pc xml:id="A55596-001-a-0830">,</pc>
     </salute>
    </opener>
    <p xml:id="A55596-e160">
     <w lemma="the" pos="d" xml:id="A55596-001-a-0840">THE</w>
     <w lemma="restitution" pos="n1" xml:id="A55596-001-a-0850">Restitution</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-0860">of</w>
     <w lemma="our" pos="po" xml:id="A55596-001-a-0870">our</w>
     <w lemma="religion" pos="n1" xml:id="A55596-001-a-0880">Religion</w>
     <pc xml:id="A55596-001-a-0890">,</pc>
     <w lemma="law" pos="n2" xml:id="A55596-001-a-0900">Laws</w>
     <pc xml:id="A55596-001-a-0910">,</pc>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-0920">and</w>
     <w lemma="liberty" pos="n2" xml:id="A55596-001-a-0930">Liberties</w>
     <pc xml:id="A55596-001-a-0940">,</pc>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-0950">and</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-0960">the</w>
     <w lemma="free" pos="vvg" xml:id="A55596-001-a-0970">freeing</w>
     <w lemma="we" pos="pno" xml:id="A55596-001-a-0980">us</w>
     <w lemma="from" pos="acp" xml:id="A55596-001-a-0990">from</w>
     <w lemma="that" pos="d" xml:id="A55596-001-a-1000">that</w>
     <w lemma="thraldom" pos="n1" xml:id="A55596-001-a-1010">Thraldom</w>
     <w lemma="which" pos="crq" xml:id="A55596-001-a-1020">which</w>
     <w lemma="have" pos="vvz" xml:id="A55596-001-a-1030">hath</w>
     <w lemma="render" pos="vvn" reg="rendered" xml:id="A55596-001-a-1040">rendred</w>
     <w lemma="we" pos="pno" xml:id="A55596-001-a-1050">us</w>
     <pc xml:id="A55596-001-a-1060">,</pc>
     <w lemma="for" pos="acp" xml:id="A55596-001-a-1070">for</w>
     <w lemma="many" pos="d" xml:id="A55596-001-a-1080">many</w>
     <w lemma="year" pos="n2" xml:id="A55596-001-a-1090">years</w>
     <w lemma="useless" pos="j" xml:id="A55596-001-a-1100">useless</w>
     <pc xml:id="A55596-001-a-1110">,</pc>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-1120">and</w>
     <w lemma="at" pos="acp" xml:id="A55596-001-a-1130">at</w>
     <w lemma="last" pos="ord" xml:id="A55596-001-a-1140">last</w>
     <w lemma="dangerous" pos="j" xml:id="A55596-001-a-1150">dangerous</w>
     <w lemma="to" pos="acp" xml:id="A55596-001-a-1160">to</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-1170">the</w>
     <w lemma="common" pos="j" xml:id="A55596-001-a-1180">Common</w>
     <w lemma="interest" pos="n1" xml:id="A55596-001-a-1190">interest</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-1200">of</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-1210">the</w>
     <w lemma="protestant" pos="jnn" xml:id="A55596-001-a-1220">Protestant</w>
     <w lemma="world" pos="n1" xml:id="A55596-001-a-1230">World</w>
     <pc xml:id="A55596-001-a-1240">,</pc>
     <w lemma="by" pos="acp" xml:id="A55596-001-a-1250">by</w>
     <w lemma="your" pos="po" xml:id="A55596-001-a-1260">your</w>
     <w lemma="highness" pos="ng1" reg="Highness'" xml:id="A55596-001-a-1270">Highness's</w>
     <w lemma="singular" pos="j" xml:id="A55596-001-a-1280">singular</w>
     <w lemma="wisdom" pos="n1" xml:id="A55596-001-a-1290">Wisdom</w>
     <pc xml:id="A55596-001-a-1300">,</pc>
     <w lemma="courage" pos="n1" xml:id="A55596-001-a-1310">Courage</w>
     <pc xml:id="A55596-001-a-1320">,</pc>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-1330">and</w>
     <w lemma="conduct" pos="vvb" xml:id="A55596-001-a-1340">Conduct</w>
     <pc xml:id="A55596-001-a-1350">,</pc>
     <w lemma="be" pos="vvb" xml:id="A55596-001-a-1360">are</w>
     <w lemma="not" pos="xx" xml:id="A55596-001-a-1370">not</w>
     <w lemma="only" pos="av-j" xml:id="A55596-001-a-1380">only</w>
     <w lemma="a" pos="d" xml:id="A55596-001-a-1390">a</w>
     <w lemma="stupendous" pos="j" xml:id="A55596-001-a-1400">stupendious</w>
     <w lemma="evidence" pos="n1" xml:id="A55596-001-a-1410">Evidence</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-1420">of</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-1430">the</w>
     <w lemma="divine" pos="j" xml:id="A55596-001-a-1440">divine</w>
     <w lemma="favour" pos="n1" xml:id="A55596-001-a-1450">favour</w>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-1460">and</w>
     <w lemma="providence" pos="n1" xml:id="A55596-001-a-1470">providence</w>
     <w lemma="for" pos="acp" xml:id="A55596-001-a-1480">for</w>
     <w lemma="our" pos="po" xml:id="A55596-001-a-1490">our</w>
     <w lemma="preservation" pos="n1" xml:id="A55596-001-a-1500">preservation</w>
     <pc xml:id="A55596-001-a-1510">;</pc>
     <w lemma="but" pos="acp" xml:id="A55596-001-a-1520">but</w>
     <w lemma="will" pos="vmb" xml:id="A55596-001-a-1530">will</w>
     <w lemma="be" pos="vvi" xml:id="A55596-001-a-1540">be</w>
     <pc xml:id="A55596-001-a-1550">,</pc>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-1560">and</w>
     <w lemma="aught" pos="pi" reg="aught" xml:id="A55596-001-a-1570">ought</w>
     <w lemma="to" pos="prt" xml:id="A55596-001-a-1580">to</w>
     <w lemma="be" pos="vvi" xml:id="A55596-001-a-1590">be</w>
     <w lemma="a" pos="d" xml:id="A55596-001-a-1600">an</w>
     <w lemma="everlasting" pos="j" xml:id="A55596-001-a-1610">Everlasting</w>
     <w lemma="monument" pos="n1" xml:id="A55596-001-a-1620">Monument</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-1630">of</w>
     <w lemma="your" pos="po" xml:id="A55596-001-a-1640">your</w>
     <w lemma="highness" pos="ng1" reg="Highness'" xml:id="A55596-001-a-1650">Highness's</w>
     <w lemma="magnimity" pos="n1" xml:id="A55596-001-a-1660">Magnimity</w>
     <pc xml:id="A55596-001-a-1670">,</pc>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-1680">and</w>
     <w lemma="other" pos="d" xml:id="A55596-001-a-1690">other</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-1700">the</w>
     <w lemma="heroic" pos="j" reg="Heroic" xml:id="A55596-001-a-1710">Heroick</w>
     <w lemma="virtue" pos="n2" xml:id="A55596-001-a-1720">Virtues</w>
     <w lemma="which" pos="crq" xml:id="A55596-001-a-1730">which</w>
     <w lemma="adorn" pos="vvb" xml:id="A55596-001-a-1740">adorn</w>
     <w lemma="your" pos="po" xml:id="A55596-001-a-1750">your</w>
     <w lemma="great" pos="j" xml:id="A55596-001-a-1760">Great</w>
     <w lemma="soul" pos="n1" xml:id="A55596-001-a-1770">Soul</w>
     <pc xml:id="A55596-001-a-1780">,</pc>
     <w lemma="by" pos="acp" xml:id="A55596-001-a-1790">by</w>
     <w lemma="who" pos="crq" xml:id="A55596-001-a-1800">whom</w>
     <w lemma="such" pos="d" xml:id="A55596-001-a-1810">such</w>
     <w lemma="a" pos="d" xml:id="A55596-001-a-1820">a</w>
     <w lemma="revolution" pos="n1" xml:id="A55596-001-a-1830">Revolution</w>
     <w lemma="be" pos="vvz" xml:id="A55596-001-a-1840">is</w>
     <w lemma="wrought" pos="vvn" xml:id="A55596-001-a-1850">wrought</w>
     <w lemma="in" pos="acp" xml:id="A55596-001-a-1860">in</w>
     <w lemma="this" pos="d" xml:id="A55596-001-a-1870">this</w>
     <w lemma="nation" pos="n1" xml:id="A55596-001-a-1880">Nation</w>
     <pc xml:id="A55596-001-a-1890">,</pc>
     <w lemma="as" pos="acp" xml:id="A55596-001-a-1900">as</w>
     <w lemma="be" pos="vvz" xml:id="A55596-001-a-1910">is</w>
     <w lemma="become" pos="vvn" xml:id="A55596-001-a-1920">become</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-1930">the</w>
     <w lemma="joy" pos="n1" xml:id="A55596-001-a-1940">Joy</w>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-1950">and</w>
     <w lemma="comfort" pos="n1" xml:id="A55596-001-a-1960">Comfort</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-1970">of</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-1980">the</w>
     <w lemma="present" pos="j" xml:id="A55596-001-a-1990">Present</w>
     <pc xml:id="A55596-001-a-2000">,</pc>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-2010">and</w>
     <w lemma="will" pos="vmb" xml:id="A55596-001-a-2020">will</w>
     <w lemma="be" pos="vvi" xml:id="A55596-001-a-2030">be</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-2040">the</w>
     <w lemma="wonder" pos="n1" xml:id="A55596-001-a-2050">Wonder</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-2060">of</w>
     <w lemma="all" pos="d" xml:id="A55596-001-a-2070">all</w>
     <w lemma="succeed" pos="vvg" xml:id="A55596-001-a-2080">Succeeding</w>
     <w lemma="age" pos="n2" xml:id="A55596-001-a-2090">Ages</w>
     <pc unit="sentence" xml:id="A55596-001-a-2100">.</pc>
    </p>
    <p xml:id="A55596-e170">
     <w lemma="in" pos="acp" xml:id="A55596-001-a-2110">In</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-2120">the</w>
     <w lemma="contrivance" pos="n1" xml:id="A55596-001-a-2130">Contrivance</w>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-2140">and</w>
     <w lemma="preparation" pos="n1" xml:id="A55596-001-a-2150">Preparation</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-2160">of</w>
     <w lemma="which" pos="crq" xml:id="A55596-001-a-2170">which</w>
     <w lemma="great" pos="j" xml:id="A55596-001-a-2180">Great</w>
     <w lemma="work" pos="n1" xml:id="A55596-001-a-2190">Work</w>
     <pc xml:id="A55596-001-a-2200">,</pc>
     <w lemma="your" pos="po" xml:id="A55596-001-a-2210">your</w>
     <w lemma="highness" pos="n1" xml:id="A55596-001-a-2220">Highness</w>
     <pc join="right" xml:id="A55596-001-a-2230">(</pc>
     <w lemma="like" pos="acp" xml:id="A55596-001-a-2240">like</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-2250">the</w>
     <w lemma="heaven" pos="n2" xml:id="A55596-001-a-2260">Heavens</w>
     <pc xml:id="A55596-001-a-2270">)</pc>
     <w lemma="do" pos="vvd" xml:id="A55596-001-a-2280">did</w>
     <w lemma="shed" pos="vvn" xml:id="A55596-001-a-2290">shed</w>
     <w lemma="your" pos="po" xml:id="A55596-001-a-2300">your</w>
     <w lemma="propitious" pos="j" xml:id="A55596-001-a-2310">propitious</w>
     <w lemma="influence" pos="n2" xml:id="A55596-001-a-2320">Influences</w>
     <w lemma="upon" pos="acp" xml:id="A55596-001-a-2330">upon</w>
     <w lemma="we" pos="pno" xml:id="A55596-001-a-2340">us</w>
     <pc xml:id="A55596-001-a-2350">,</pc>
     <w lemma="whilst" pos="cs" xml:id="A55596-001-a-2360">whilst</w>
     <w lemma="we" pos="pns" xml:id="A55596-001-a-2370">we</w>
     <w lemma="sleep" pos="vvd" xml:id="A55596-001-a-2380">slept</w>
     <pc xml:id="A55596-001-a-2390">,</pc>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-2400">and</w>
     <w lemma="have" pos="vvd" xml:id="A55596-001-a-2410">had</w>
     <w lemma="scarce" pos="av-j" xml:id="A55596-001-a-2420">scarce</w>
     <w lemma="any" pos="d" xml:id="A55596-001-a-2430">any</w>
     <w lemma="prospect" pos="n1" xml:id="A55596-001-a-2440">Prospect</w>
     <w lemma="from" pos="acp" xml:id="A55596-001-a-2450">from</w>
     <w lemma="whence" pos="crq" xml:id="A55596-001-a-2460">whence</w>
     <w lemma="we" pos="pns" xml:id="A55596-001-a-2470">we</w>
     <w lemma="may" pos="vmd" xml:id="A55596-001-a-2480">might</w>
     <w lemma="expect" pos="vvi" xml:id="A55596-001-a-2490">expect</w>
     <w lemma="our" pos="po" xml:id="A55596-001-a-2500">our</w>
     <w lemma="redemption" pos="n1" xml:id="A55596-001-a-2510">Redemption</w>
     <pc unit="sentence" xml:id="A55596-001-a-2520">.</pc>
    </p>
    <p xml:id="A55596-e180">
     <w lemma="but" pos="acp" xml:id="A55596-001-a-2530">But</w>
     <w lemma="as" pos="acp" xml:id="A55596-001-a-2540">as</w>
     <w lemma="since" pos="acp" xml:id="A55596-001-a-2550">since</w>
     <w lemma="your" pos="po" xml:id="A55596-001-a-2560">your</w>
     <w lemma="happy" pos="j" xml:id="A55596-001-a-2570">happy</w>
     <w lemma="arrival" pos="n1" xml:id="A55596-001-a-2580">Arrival</w>
     <w lemma="in" pos="acp" xml:id="A55596-001-a-2590">in</w>
     <hi xml:id="A55596-e190">
      <w lemma="England" pos="nn1" xml:id="A55596-001-a-2600">England</w>
      <pc xml:id="A55596-001-a-2610">,</pc>
     </hi>
     <w lemma="we" pos="pns" xml:id="A55596-001-a-2620">we</w>
     <w lemma="do" pos="vvd" xml:id="A55596-001-a-2630">did</w>
     <w lemma="among" pos="acp" xml:id="A55596-001-a-2640">amongst</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-2650">the</w>
     <w lemma="first" pos="ord" xml:id="A55596-001-a-2660">first</w>
     <pc xml:id="A55596-001-a-2670">,</pc>
     <w lemma="associate" pos="vvi" xml:id="A55596-001-a-2680">Associate</w>
     <w lemma="ourselves" pos="pr" reg="ourselves" xml:id="A55596-001-a-2690">our selves</w>
     <w lemma="to" pos="prt" xml:id="A55596-001-a-2710">to</w>
     <w lemma="assist" pos="vvi" xml:id="A55596-001-a-2720">assist</w>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-2730">and</w>
     <w lemma="promote" pos="vvi" xml:id="A55596-001-a-2740">promote</w>
     <w lemma="your" pos="po" xml:id="A55596-001-a-2750">your</w>
     <w lemma="highness" pos="ng1" reg="Highness'" xml:id="A55596-001-a-2760">Highness's</w>
     <w lemma="most" pos="avs-d" xml:id="A55596-001-a-2770">Most</w>
     <w lemma="glorious" pos="j" xml:id="A55596-001-a-2780">Glorious</w>
     <w lemma="design" pos="n1" xml:id="A55596-001-a-2790">Design</w>
     <pc xml:id="A55596-001-a-2800">,</pc>
     <w lemma="with" pos="acp" xml:id="A55596-001-a-2810">with</w>
     <w lemma="our" pos="po" xml:id="A55596-001-a-2820">our</w>
     <w lemma="life" pos="n2" xml:id="A55596-001-a-2830">Lives</w>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-2840">and</w>
     <w lemma="fortune" pos="n2" xml:id="A55596-001-a-2850">Fortunes</w>
     <pc xml:id="A55596-001-a-2860">,</pc>
     <w lemma="to" pos="acp" xml:id="A55596-001-a-2870">to</w>
     <w lemma="which" pos="crq" xml:id="A55596-001-a-2880">which</w>
     <w lemma="we" pos="pns" xml:id="A55596-001-a-2890">we</w>
     <w lemma="think" pos="vvb" xml:id="A55596-001-a-2900">think</w>
     <w lemma="ourselves" pos="pr" reg="ourselves" xml:id="A55596-001-a-2910">our selves</w>
     <w lemma="bind" pos="vvn" xml:id="A55596-001-a-2930">bound</w>
     <w lemma="in" pos="acp" xml:id="A55596-001-a-2940">in</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-2950">the</w>
     <w lemma="high" pos="js" xml:id="A55596-001-a-2960">highest</w>
     <w lemma="obligation" pos="n1" xml:id="A55596-001-a-2970">Obligation</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-2980">of</w>
     <w lemma="gratitude" pos="n1" xml:id="A55596-001-a-2990">Gratitude</w>
     <pc xml:id="A55596-001-a-3000">,</pc>
     <w lemma="most" pos="avs-d" xml:id="A55596-001-a-3010">most</w>
     <w lemma="humble" pos="av-j" xml:id="A55596-001-a-3020">humbly</w>
     <w lemma="to" pos="prt" xml:id="A55596-001-a-3030">to</w>
     <w lemma="present" pos="vvi" xml:id="A55596-001-a-3040">present</w>
     <w lemma="to" pos="acp" xml:id="A55596-001-a-3050">to</w>
     <w lemma="your" pos="po" xml:id="A55596-001-a-3060">your</w>
     <w lemma="highness" pos="n1" xml:id="A55596-001-a-3070">Highness</w>
     <pc xml:id="A55596-001-a-3080">,</pc>
     <w lemma="our" pos="po" xml:id="A55596-001-a-3090">our</w>
     <w lemma="humble" pos="j" xml:id="A55596-001-a-3100">humble</w>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-3110">and</w>
     <w lemma="hearty" pos="j" xml:id="A55596-001-a-3120">hearty</w>
     <w lemma="thanks" pos="n2" xml:id="A55596-001-a-3130">Thanks</w>
     <pc xml:id="A55596-001-a-3140">,</pc>
     <w lemma="for" pos="acp" xml:id="A55596-001-a-3150">for</w>
     <w lemma="this" pos="d" xml:id="A55596-001-a-3160">this</w>
     <w lemma="our" pos="po" xml:id="A55596-001-a-3170">our</w>
     <w lemma="deliverance" pos="n1" xml:id="A55596-001-a-3180">Deliverance</w>
     <w lemma="from" pos="acp" xml:id="A55596-001-a-3190">from</w>
     <w lemma="popery" pos="n1" xml:id="A55596-001-a-3200">Popery</w>
     <pc xml:id="A55596-001-a-3210">,</pc>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-3220">and</w>
     <w lemma="arbitrary" pos="j" xml:id="A55596-001-a-3230">Arbitrary</w>
     <w lemma="power" pos="n1" xml:id="A55596-001-a-3240">Power</w>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-3250">and</w>
     <w lemma="likewise" pos="av" xml:id="A55596-001-a-3260">likewise</w>
     <pc xml:id="A55596-001-a-3270">,</pc>
     <w lemma="for" pos="acp" xml:id="A55596-001-a-3280">for</w>
     <w lemma="declare" pos="vvg" xml:id="A55596-001-a-3290">declaring</w>
     <w lemma="your" pos="po" xml:id="A55596-001-a-3300">your</w>
     <w lemma="gracious" pos="j" xml:id="A55596-001-a-3310">Gracious</w>
     <w lemma="intention" pos="n2" xml:id="A55596-001-a-3320">Intentions</w>
     <pc xml:id="A55596-001-a-3330">,</pc>
     <w lemma="that" pos="cs" xml:id="A55596-001-a-3340">That</w>
     <w lemma="by" pos="acp" xml:id="A55596-001-a-3350">by</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-3360">the</w>
     <w lemma="advice" pos="n1" xml:id="A55596-001-a-3370">Advice</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-3380">of</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-3390">the</w>
     <w lemma="estate" pos="n2" xml:id="A55596-001-a-3400">Estates</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-3410">of</w>
     <w lemma="this" pos="d" xml:id="A55596-001-a-3420">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A55596-001-a-3430">Kingdom</w>
     <pc xml:id="A55596-001-a-3440">,</pc>
     <w lemma="you" pos="pn" xml:id="A55596-001-a-3450">you</w>
     <w lemma="will" pos="vmb" xml:id="A55596-001-a-3460">will</w>
     <w lemma="rectify" pos="vvi" reg="rectify" xml:id="A55596-001-a-3470">rectifie</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-3480">the</w>
     <w lemma="late" pos="j" xml:id="A55596-001-a-3490">late</w>
     <w lemma="disorder" pos="n2" xml:id="A55596-001-a-3500">Disorders</w>
     <w lemma="in" pos="acp" xml:id="A55596-001-a-3510">in</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-3520">the</w>
     <w lemma="government" pos="n1" xml:id="A55596-001-a-3530">Government</w>
     <pc xml:id="A55596-001-a-3540">,</pc>
     <w lemma="both" pos="d" xml:id="A55596-001-a-3550">both</w>
     <w lemma="ecclesiastical" pos="j" xml:id="A55596-001-a-3560">Ecclesiastical</w>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-3570">and</w>
     <w lemma="civil" pos="j" xml:id="A55596-001-a-3580">Civil</w>
     <pc xml:id="A55596-001-a-3590">,</pc>
     <w lemma="according" pos="j" xml:id="A55596-001-a-3600">according</w>
     <w lemma="to" pos="acp" xml:id="A55596-001-a-3610">to</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-3620">the</w>
     <w lemma="know" pos="j-vn" xml:id="A55596-001-a-3630">Known</w>
     <w lemma="law" pos="n2" xml:id="A55596-001-a-3640">Laws</w>
     <pc unit="sentence" xml:id="A55596-001-a-3650">.</pc>
     <w lemma="the" pos="d" xml:id="A55596-001-a-3660">The</w>
     <w lemma="due" pos="j" xml:id="A55596-001-a-3670">due</w>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-3680">and</w>
     <w lemma="inviolable" pos="j" xml:id="A55596-001-a-3690">inviolable</w>
     <w lemma="observation" pos="n1" xml:id="A55596-001-a-3700">Observation</w>
     <w lemma="of" pos="acp" xml:id="A55596-001-a-3710">of</w>
     <w lemma="which" pos="crq" xml:id="A55596-001-a-3720">which</w>
     <pc xml:id="A55596-001-a-3730">,</pc>
     <w lemma="will" pos="n1" xml:id="A55596-001-a-3740">will</w>
     <pc xml:id="A55596-001-a-3750">,</pc>
     <w lemma="in" pos="acp" xml:id="A55596-001-a-3760">in</w>
     <w lemma="our" pos="po" xml:id="A55596-001-a-3770">our</w>
     <w lemma="poor" pos="j" xml:id="A55596-001-a-3780">poor</w>
     <w lemma="opinion" pos="n1" xml:id="A55596-001-a-3790">Opinion</w>
     <pc xml:id="A55596-001-a-3800">,</pc>
     <w lemma="be" pos="vvb" xml:id="A55596-001-a-3810">be</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-3820">the</w>
     <w lemma="only" pos="j" xml:id="A55596-001-a-3830">only</w>
     <w lemma="proper" pos="j" xml:id="A55596-001-a-3840">proper</w>
     <w lemma="mean" pos="n2" xml:id="A55596-001-a-3850">Means</w>
     <w lemma="to" pos="prt" xml:id="A55596-001-a-3860">to</w>
     <w lemma="render" pos="vvi" xml:id="A55596-001-a-3870">render</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-3880">the</w>
     <w lemma="sovereign" pos="n1-j" reg="Sovereign" xml:id="A55596-001-a-3890">Soveraign</w>
     <w lemma="secure" pos="j" xml:id="A55596-001-a-3900">secure</w>
     <pc xml:id="A55596-001-a-3910">,</pc>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-3920">and</w>
     <w lemma="both" pos="d" xml:id="A55596-001-a-3930">both</w>
     <w lemma="sovereign" pos="j" reg="Sovereign" xml:id="A55596-001-a-3940">Soveraign</w>
     <w lemma="and" pos="cc" xml:id="A55596-001-a-3950">and</w>
     <w lemma="subject" pos="j" xml:id="A55596-001-a-3960">Subject</w>
     <w lemma="happy" pos="j" xml:id="A55596-001-a-3970">happy</w>
     <pc unit="sentence" xml:id="A55596-001-a-3980">.</pc>
    </p>
    <p xml:id="A55596-e200">
     <hi xml:id="A55596-e210">
      <w lemma="to" pos="acp" xml:id="A55596-001-a-3990">To</w>
      <w lemma="which" pos="crq" xml:id="A55596-001-a-4000">which</w>
      <w lemma="his" pos="po" xml:id="A55596-001-a-4010">His</w>
      <w lemma="highness" pos="n1" xml:id="A55596-001-a-4020">Highness</w>
      <w lemma="return" pos="vvd" reg="returned" xml:id="A55596-001-a-4030">return'd</w>
      <w lemma="a" pos="d" xml:id="A55596-001-a-4040">a</w>
      <w lemma="most" pos="avs-d" xml:id="A55596-001-a-4050">most</w>
      <w lemma="gracious" pos="j" xml:id="A55596-001-a-4060">Gracious</w>
      <w lemma="answer" pos="n1" xml:id="A55596-001-a-4070">Answer</w>
      <pc unit="sentence" xml:id="A55596-001-a-4080">.</pc>
     </hi>
    </p>
   </div>
  </body>
  <back xml:id="A55596-e220">
   <div type="colophon" xml:id="A55596-e230">
    <p xml:id="A55596-e240">
     <hi xml:id="A55596-e250">
      <w lemma="LONDON" pos="nn1" xml:id="A55596-001-a-4090">LONDON</w>
      <pc xml:id="A55596-001-a-4100">:</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A55596-001-a-4110">Printed</w>
     <w lemma="in" pos="acp" xml:id="A55596-001-a-4120">in</w>
     <w lemma="the" pos="d" xml:id="A55596-001-a-4130">the</w>
     <w lemma="year" pos="n1" xml:id="A55596-001-a-4140">Year</w>
     <w lemma="1689." pos="crd" xml:id="A55596-001-a-4150">1689.</w>
     <pc unit="sentence" xml:id="A55596-001-a-4160"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
